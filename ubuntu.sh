#/bin/bash
VERSION=lsb_release -sr
echo "$(dpkg-query -W -f="\${binary:Package},\${db:Status-Abbrev},\${Version},\${Source},\${source:Version}\n")" > /tmp/software-list-2751.txt
curl -X POST -H "Session: $1" -H "Content-Type: text/plain" -H "X-Vuls-OS-Family: `hostname`" -H "X-Vuls-OS-Release: $VERSION" -H "X-Vuls-Kernel-Release: `uname -r`" --data-binary @/tmp/software-list-2751.txt http://${VULS_SERVER}:5515/vuls
rm /tmp/software-list-2751.txt
